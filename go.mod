module unraid-docker-webui

go 1.20

require github.com/goccy/go-yaml v1.9.5

require (
	github.com/fatih/color v1.13.0 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/sys v0.0.0-20220731174439-a90be440212d // indirect
	golang.org/x/xerrors v0.0.0-20220609144429-65e65417b02f // indirect
)
