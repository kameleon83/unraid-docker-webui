<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{.Title}}</title>
    <noscript>
        For full functionality of this site it is necessary to enable JavaScript.
        Here are the <a href="https://www.enable-javascript.com/">
            instructions how to enable JavaScript in your web browser</a>.
    </noscript>
    <link rel="icon" href="/static/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/static/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700;800;900&display=swap"
          rel="stylesheet">
    <link rel="manifest" href="../static/manifest.json" crossorigin="use-credentials"/>
    <style>
        * {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
        }

        :root {
            font-family: 'Poppins', sans-serif;
            --primary: #1e252f;
            --text: #eee5e9;
            --radius: 12px;
            color: var(--text);
        }

        body {
            text-align: center;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-wrap: wrap;
            background-color: var(--primary);
            min-height: 100vh;
        }


        a {
            color: inherit;
            text-decoration: none;
            display: block;
            width: 100%;
        }

        /*div#message {*/
        /*    width: 100vw;*/
        /*    height: 50px;*/
        /*    background-color: transparent;*/
        /*    color: transparent;*/
        /*    display: flex;*/
        /*    justify-content: center;*/
        /*    align-items: center;*/
        /*    text-align: center;*/
        /*    position: fixed;*/
        /*    top: 0;*/
        /*    left: 0;*/
        /*    right: 0;*/
        /*    transform: translateY(-50px);*/
        /*    font-size: 1.2em;*/
        /*    z-index: 9999;*/
        /*    transition: transform 300ms ease-in-out;*/
        /*}*/

        section {
            position: relative;
            width: 100vw;
            height: 100vh;
            transition: transform 300ms ease-in-out;
        }

        {{if eq .IsRound "yes"}}
        section::before {
            content: '';
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            clip-path: circle(25% at right 60%);
            background: linear-gradient(#f00, #f0f);
        }

        section::after {
            content: '';
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            background: linear-gradient(#2785ce, #e91e63);
            clip-path: circle(20% at 10% 10%);
        }

        {{end}}

        #div-zoom {
            position: fixed;
            z-index: 9999;
            top: 49%;
            bottom: 49%;
            left: -120px;
            transform: rotateZ(-90deg);
            font-size: 1.2rem;
        }

        #div-zoom .rotate {
            position: absolute;
            top: -8px;
            right: -40px;
            /*left: 500px;*/
            transform: rotate(45deg);
            cursor: pointer;
        }

        #div-zoom #zoom {
            width: 300px;
            -webkit-appearance: none; /* Override default CSS styles */
            appearance: none;
            height: 15px; /* Specified height */
            background: #d3d3d3; /* Grey background */
            outline: none; /* Remove outline */
            -webkit-transition: .2s; /* 0.2 seconds transition on hover */
            transition: opacity .2s;
            color: #eee5e9;
            background: rgba(255, 255, 255, 0.06);
            box-shadow: 0 15px 35px rgba(0, 0, 0, 0.2);
            border-radius: var(--radius);
            border: 1px solid rgba(255, 255, 255, 0.1);
            backdrop-filter: blur(10px) saturate(180%);
        }

        #div-zoom #zoom:hover {
            opacity: 1; /* Fully shown on mouse-over */
        }

        #div-zoom #zoom::-webkit-slider-thumb {
            -webkit-appearance: none; /* Override default look */
            appearance: none;
            width: 25px; /* Set a specific slider handle width */
            height: 25px; /* Slider handle height */
            background: linear-gradient(#2785ce, #e91e63);
            border-radius: 13px;
            cursor: pointer; /* Cursor on hover */
        }

        #div-zoom #zoom::-moz-range-thumb {
            width: 25px; /* Set a specific slider handle width */
            height: 25px; /* Slider handle height */
            background: linear-gradient(#2785ce, #e91e63);
            border-radius: 13px;
            cursor: pointer; /* Cursor on hover */
        }

        .container {
            position: relative;
            z-index: 1;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-wrap: wrap;
            margin: 20px 0;
            transform-origin: top left;
        }

        .container #filter {
            width: 400px;
            height: 50px;
            font-size: 1.2em;
            text-align: center;
            color: #eee5e9;
            background: rgba(255, 255, 255, 0.06);
            box-shadow: 0 15px 35px rgba(0, 0, 0, 0.2);
            margin: 20px;
            padding: 5px;
            border-radius: var(--radius);
            border: 1px solid rgba(255, 255, 255, 0.1);
            backdrop-filter: blur(10px) saturate(180%);
        }

        .container #filter:focus {
            outline: none;
            background: rgba(255, 255, 255, 0.03);
            border: 1px solid rgba(255, 255, 255, 0.1);
        }

        .container #filter::placeholder {
            font-size: 1.3em;
            color: rgba(238, 229, 233, 0.8);
            text-align: center;
        }

        .container #filter:focus::placeholder {
            opacity: 0.3;
        }

        .container h3 {
            width: 100%;
        }

        .container h5 {
            width: 100%;
            font-weight: 200;
            margin-top: 5px;
        }

        .container .card {
            position: relative;
            width: 18em;
            height: 12em;
            background: rgba(255, 255, 255, 0.06);
            box-shadow: 0 15px 35px rgba(0, 0, 0, 0.2);
            margin: 18px;
            padding: 5px;
            border-radius: var(--radius);
            border: 1px solid rgba(255, 255, 255, 0.1);
            backdrop-filter: blur(10px) saturate(180%);
            font-size: 1.3em;
            font-weight: 700;
        }

        .container hr {
            width: 80%;
            margin: 20px;
        }

        .container > .card .content {
            position: relative;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            transition: opacity 300ms;
            width: 100%;
        }

        .container .card.running .content {
            height: {{if .Wan}} calc(100% - 2.4em) {{else}} 100%{{end}};
            opacity: 0.8;
        }

        .container .card.running > a {
            height: 100%;
        }

        .container > .card.not-running .content {
            opacity: 0.4;
            height: 100%;
        }

        /*.container > .card.not-running .play {*/
        /*    position: absolute;*/
        /*    top: 0;*/
        /*    left: 0;*/
        /*    width: 100%;*/
        /*    height: 100%;*/
        /*    display: flex;*/
        /*    justify-content: center;*/
        /*    align-items: center;*/
        /*    opacity: 0;*/
        /*    visibility: hidden;*/
        /*    transition: opacity 300ms ease-in-out, visibility 300ms ease-in-out;*/
        /*}*/

        /*.container > .card.not-running:hover .play {*/
        /*    opacity: 1;*/
        /*    visibility: visible;*/
        /*    background-color: rgba(0, 0, 0, 0.2);*/
        /*    cursor: pointer;*/
        /*}*/

        .container > .card.running:hover .content,
        .container > .card.running:active .content {
            opacity: 1;
        }

        .container > .card .content div {
            margin: 10px 0;
        }

        /*.container > .card .title h3 {*/
        /*    font-size: 1.5em;*/
        /*    font-weight: 700;*/
        /*}*/

        .container > .card .content .img {
            position: relative;
            width: calc((230 / 400) * 8em);
            height: calc((230 / 400) * 8em);
            border-radius: 50%;
            overflow: hidden;
            border: 10px solid rgba(0, 0, 0, 0.25);
        }

        .container > .card .img img {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .container .card.running .dns {
            width: 100%;
            position: relative;
        }

        .container .card .dns input {
            width: 100%;
            height: 3em;
            font-size: 0.8em;
            background-color: rgba(255, 255, 255, 0.06);
            color: inherit;
            outline: none;
            text-align: center;
            padding: 5px;
            border: 1px solid rgba(255, 255, 255, 0.1);
            border-radius: var(--radius);
        }

        .container > .card .link {
            position: absolute;
            z-index: 9999;
            top: 0;
            bottom: 3em;
            left: 0;
            right: 0;
            opacity: 0;
            visibility: hidden;
            background-color: rgba(0, 0, 0, 0.7);
            border-radius: var(--radius);
            transition: opacity 200ms ease-in-out, visibility 200ms ease-in-out;
        }

        .container > .card:hover .link,
        .container > .card:active .link {
            opacity: 1;
            visibility: visible;
        }

        .container > .card .link a {
            /*border: 1px solid red;*/
            height: calc(100% / 2);
            display: flex;
            justify-content: center;
            flex-direction: column;
            align-items: center;
            font-size: 1.2em;
            transition: background-color 200ms ease-in-out;
        }

        .container > .card .link a:hover,
        .container > .card .link a:active {
            background-color: rgba(255, 255, 255, 0.5);
        }

        .container > .card .link a:first-child {
            border-bottom: 1px solid rgba(255, 255, 255, 0.5);
            border-radius: var(--radius) var(--radius) 0 0;
        }

        .container > .card .link a:last-child {
            border-radius: 0 0 var(--radius) var(--radius);
        }

    </style>
</head>
<body>
{{/*<div id="message">Test</div>*/}}
<section>
    <div id="div-zoom">
        <div class="rotate">
            <label for="zoom" class="rotate">Zoom <span id="zoom-value"></span>%</label>
        </div>
        <input type="range" id="zoom" name="zoom"
               min="50" max="150" value="100">
    </div>
    <div class="container" id="container">
        <input autofocus id="filter" name="filter" placeholder="filter" type="search">
        <label for="filter"></label>
        {{if .Running}}
            <h3>Docker App Running</h3>
            <h5>Click on any card to access its webUI</h5>
            {{ $wan := .Wan}}
            {{ range $i, $docker := .Running}}
                <div class="card running">
                    {{if $wan }}
                        <div class="content">
                            <div class="title">
                                <h3>{{ $docker.Name }}</h3>
                            </div>
                            <div class="img">
                                <img src="{{$docker.Icon}}" alt="">
                            </div>
                        </div>

                        <div class="dns">
                            <input type="text"
                                   value="{{if eq $docker.SubDomain  ""}}{{$docker.WebuiWan}}{{else}}{{$docker.SubDomain}}{{end}}"
                                   id="input_dns" placeholder="subdomains">
                        </div>
                        <div class="link">
                            <a href="{{$docker.WebuiLan}}"
                               target="_blank">
                                LAN
                            </a>
                            <a href="{{if eq $docker.SubDomain  ""}}{{$docker.WebuiWan}}{{else}}{{$docker.SubDomain}}{{end}}"
                               target="_blank">
                                WAN
                            </a>
                        </div>
                    {{ else }}
                        <a href="{{$docker.WebuiLan}}" target="_blank">
                            <div class="content">
                                <div class="title">
                                    <h3>{{ $docker.Name }}</h3>
                                </div>
                                <div class="img">
                                    <img src="{{$docker.Icon}}" alt="">
                                </div>
                            </div>
                        </a>
                    {{ end }}
                </div>
            {{ end }}
        {{ end }}
        <hr>
        {{if .NotRunning}}
            <h3>Docker App Not Running</h3>
            {{range $i, $docker := .NotRunning}}
                <div class="card not-running">
                    <div class="content">
                        <div class="title">
                            <h3>{{$docker.Name}}</h3>
                        </div>
                        <div class="img">
                            <img src="{{$docker.Icon}}" alt="">
                        </div>
                    </div>
                    {{/*                    <div class="play">*/}}
                    {{/*                        <svg style="width: 100%; height: 100%;" viewBox="0 0 100 100" preserveAspectRatio="xMaxYMax">*/}}
                    {{/*                            <polygon points="0,33 33,50 0,66" style="fill:rgba(255,255,255,0.7);"></polygon>*/}}
                    {{/*                        </svg>*/}}
                    {{/*                    </div>*/}}
                    {{/*                    <div class="dns">*/}}
                    {{/*                        <input type="text" value="{{if eq $docker.SubDomain  ""}}{{$docker.Webui}}{{else}}{{$docker.SubDomain}}{{end}}" id="input_dns" placeholder="subdomains">*/}}
                    {{/*                    </div>*/}}
                </div>
            {{ end }}
        {{ end }}
    </div>
</section>

<script>
    const zoom = document.getElementById("zoom")
    const localZoom = localStorage.getItem("size")
    const zoomValue = document.getElementById("zoom-value")

    document.body.addEventListener('load', function () {
        console.log(this.style.fontSize);
    })

    if (localZoom == null) {
        zoom.setAttribute("value", 100)
        zoomValue.innerText = "100"
    } else {
        zoom.setAttribute("value", localZoom)
        document.body.style.fontSize = (localZoom / 100) + 'rem'
        zoomValue.innerText = localZoom
    }

    const input = document.getElementById("filter")
    input.addEventListener('keyup', function () {
        const card = document.getElementsByClassName("card")
        const filter = input.value.toUpperCase()

        for (let c = 0; c < card.length; c++) {
            const title = card[c].querySelector(".title > h3")
            const up = title.innerText.toUpperCase()
            if (up.indexOf(filter) > -1) {
                card[c].style.display = ""
            } else {
                card[c].style.display = "none"
            }
        }
    })

    input.addEventListener('search', function () {
        console.log("click")
        const card = document.getElementsByClassName("card")
        for (let c = 0; c < card.length; c++) {
            card[c].style.display = ""
        }
    })

    document.getElementById('zoom').addEventListener('input', function (e) {
        const percent = parseFloat(e.target.value);
        document.body.style.fontSize = (percent / 100) + 'rem'
        zoomValue.innerText = percent.toString()
        localStorage.setItem("size", percent.toString())
    })

    document.querySelector('label.rotate').addEventListener('click', function () {
        document.body.style.fontSize = '1rem'
        zoomValue.innerText = '100'
        localStorage.setItem("size", '100')
        zoom.value = 100
    })

    // document.querySelectorAll('.card.not-running .play').forEach(svg => {
    //     svg.addEventListener('click', e => {
    //         let h3 = ""
    //         if (e.target.tagName === "svg") {
    //             h3 = e.target.parentElement.parentElement.querySelector('.card.not-running .content .title h3')
    //         } else if (e.target.tagName === "polygon") {
    //             h3 = e.target.parentElement.parentElement.parentElement.querySelector('.card.not-running .content .title h3')
    //         }
    //         console.log('h3 : ', h3)
    //         const title = h3.innerHTML
    //
    //         fetch('docker-start', {
    //             method: "POST",
    //             headers: new Headers({
    //                 'Content-Type': 'application/x-www-form-urlencoded',
    //             }),
    //             body: `title=${title}`
    //         }).then(resp => resp.json())
    //             .then(json => {
    //                 if (json.err === '' && json.message !== '') {
    //                     const message = document.getElementById('message')
    //                     const section = document.querySelector('section')
    //                     message.innerHTML = json.message + "The page reloading to 5s !"
    //                     message.style.transform = "translateY(0px)"
    //                     message.style.backgroundColor = "#3A6670"
    //                     message.style.color = "white"
    //                     section.style.transform = "translateY(50px)"
    //                     setTimeout(()=>{
    //                         fetch(json.unraid_ip+'/plugins/dynamix.docker.manager/include/DockerContainers.php').then(response => {
    //                             console.log(response)
    //                         })
    //                     },1000)
    //                     setTimeout(() => {
    //                         location.reload()
    //                     }, 5000)
    //                 } else {
    //                     const message = document.getElementById('message')
    //                     const section = document.querySelector('section')
    //                     message.innerHTML = json.err
    //                     message.style.transform = "translateY(0px)"
    //                     message.style.backgroundColor = "#E86C3E"
    //                     message.style.color = "white"
    //                     section.style.transform = "translateY(50px)"
    //                     setTimeout(() => {
    //                         message.style.transform = "translateY(-50px)"
    //                         section.style.transform = "translateY(0px)"
    //                     }, 5000)
    //                 }
    //             })
    //             .catch(err => {
    //                 console.log(err)
    //             })
    //     })
    // })
    {{ if .Wan}}
    const runs = document.querySelectorAll(".card.running")
    runs.forEach(r => {
        const title = r.querySelector('.title h3').innerHTML;
        r.querySelector('.dns').addEventListener('change', e => {
            console.log(e.target.value, title)
            const obj = {
                title: title,
                sub_domain: e.target.value
            }
            fetch("/update", {
                method: 'POST',
                body: JSON.stringify(obj)
            }).then(resp => {
                // console.log(resp.status)
                if (resp.status === 201) {
                    location.reload()
                }
            }).catch(err => {
                console.log(err)
            })
        })
    })
    {{ end }}
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('/sw.js')
                .then((registration) => {
                    console.log(`service worker registered succesfully`)
                    console.log(registration)
                })
                .catch((err) => {
                    // console.log(`Error registring`)
                    console.log(err)
                })
        })
    } else {
        console.log(`Service worker is not supported in this browser.`)
    }
</script>
</body>
</html>
